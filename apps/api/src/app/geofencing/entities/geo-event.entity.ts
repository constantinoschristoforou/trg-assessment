import { Document } from 'mongoose';
import { GeoEventType } from './geo-event-type.enum';

export interface GeoEventEntity extends Document {
  device: string;
  type: GeoEventType,
  forEntity: 'device' | 'area';
  data?: Object;
}
