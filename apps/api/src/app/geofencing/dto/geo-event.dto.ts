import { IsNotEmpty } from 'class-validator';
import { GeoEventType } from '../entities/geo-event-type.enum';

export class GeoEventDto {

  @IsNotEmpty()
  deviceId: string;

  @IsNotEmpty()
  type: GeoEventType;

  data?: Object;

  forEntity: 'device' | 'area';

  constructor(data: Partial<GeoEventDto>) {
    Object.assign(this, data);
  }
}
