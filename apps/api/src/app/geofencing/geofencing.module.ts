import { Module } from '@nestjs/common';
import { GeoEventProviders } from './geo-event.providers';
import { GeoEventService } from './geo-event.service';
import { DatabaseModule } from '../database/database.module';
import { GeofencingHandler } from './commands/handlers/geofencing.handler';
import { DeviceModule } from '../devices/device.module';
import { AreasModule } from '../areas/areas.module';
import { PushGeoEventCommand } from './commands/push-geo-event.command';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [
    CqrsModule,
    DatabaseModule,
    DeviceModule,
    AreasModule
  ],
  providers: [
    GeoEventService,
    ...GeoEventProviders,
    GeofencingHandler,
    PushGeoEventCommand
  ],
  exports: [
    GeoEventService
  ]
})
export class GeofencingModule {
}
