import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { GeofencingCommand } from '../../../devices/commands/geofencing.command';
import { AreaService } from '../../../areas/area.service';
import { DeviceService } from '../../../devices/device.service';
import { GeoEventDto } from '../../dto/geo-event.dto';
import { GeoEventType } from '../../entities/geo-event-type.enum';
import { GeoEventService } from '../../geo-event.service';

@CommandHandler(GeofencingCommand)
export class GeofencingHandler implements ICommandHandler<GeofencingCommand> {
  constructor(private areaService: AreaService,
              private geoEventService: GeoEventService,
              private deviceService: DeviceService) {
  }

  async execute(command: GeofencingCommand) {
    this._produceInAreaEvents(command);
    this._produceNearByAreaEvents(command);
    this._produceNearByDeviceEvents(command);
  }

  private async _produceInAreaEvents(command: GeofencingCommand) {
    const lngLat = command.location.point.coordinates;
    const deviceId = command.location.device;

    const areas = await this.areaService.withinAreas(lngLat).toPromise();

    if (areas.length) {
      const device = await this.deviceService.find(deviceId).toPromise();

      areas.forEach(area => {
        const event = new GeoEventDto({
          type: GeoEventType.IN,
          deviceId: command.location.device,
          forEntity: 'area',
          data: {
            area: {
              _id: area._id,
              name: area.name
            },
            point: lngLat,
            device: {
              _id: device._id,
              name: device.name
            }
          }
        });
        this.geoEventService.create(event);
      });

    }
  }

  private async _produceNearByAreaEvents(command: GeofencingCommand) {
    const lngLat = command.location.point.coordinates;
    const deviceId = command.location.device;

    const areas = await this.areaService.nearByAreas(lngLat).toPromise();

    if (areas.length) {
      const device = await this.deviceService.find(deviceId).toPromise();
      areas.forEach(area => {
        const event = new GeoEventDto({
          type: GeoEventType.NEAR,
          deviceId: deviceId,
          forEntity: 'area',
          data: {
            area: {
              _id: area._id,
              name: area.name
            },
            point: lngLat,
            device: {
              _id: device._id,
              name: device.name
            },
            distance: area.dist
          }
        });
        this.geoEventService.create(event);
      });
    }
  }

  private async _produceNearByDeviceEvents(command: GeofencingCommand) {

    const lngLat = command.location.point.coordinates;
    const deviceId = command.location.device;
    const devices = await this.deviceService.nearByDevices(lngLat, 1000, [deviceId]).toPromise();
    const device = await this.deviceService.find(deviceId).toPromise();

    devices.forEach(nearDevice => {
      const event = new GeoEventDto({
        type: GeoEventType.NEAR,
        deviceId: deviceId,
        forEntity: 'device',
        data: {
          point: lngLat,
          device: {
            _id: device._id,
            name: device.name
          },
          nearDevice: {
            _id: nearDevice._id,
            name: nearDevice.name
          },
          distance: nearDevice.dist
        }
      });
      this.geoEventService.create(event);
    });

  }


}


//
// async execute(command: GeofencingCommand) {
//
//   const lngLat = command.location.point.coordinates;
//   const deviceId = command.location.device;
//
//   const areas = await this.areaService.belongToArea(lngLat).toPromise();
//
//   if (areas.length) {
//     const device = await this.deviceService.find(deviceId).toPromise();
//
//     areas.forEach(area => {
//
//       const alert = new AlertDto({
//         name: device.name + ' in ' + area.name,
//         type: AlertType.GEOFENCE,
//         deviceId: command.location.device,
//         data: {
//           area: {
//             _id: area._id,
//             name: area.name
//           },
//           point: lngLat,
//           device: {
//             _id: device._id,
//             name: device.name
//           }
//         }
//       });
//
//       this.alertService.create(alert);
//     });
//
//   }
//
// }
