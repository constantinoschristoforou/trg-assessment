import { GeoEventEntity } from '../entities/geo-event.entity';

export class PushGeoEventCommand {
  constructor(
    public readonly geoEvent: GeoEventEntity) {
  }
}
