import * as mongoose from 'mongoose';
import { GeoEventType } from '../entities/geo-event-type.enum';


export const GeoEventSchema = new mongoose.Schema({
    device: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Device'
    },
    type: {
      type: String,
      enum: [GeoEventType.IN, GeoEventType.NEAR]
    },
    forEntity: {
      type: String,
      enum: ['device', 'area']
    },
    data: Object
  },
  {
    timestamps: true
  });
