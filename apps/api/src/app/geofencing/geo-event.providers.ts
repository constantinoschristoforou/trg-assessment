import { Connection } from 'mongoose';
import { GEO_EVENT_MODEL } from './constants';
import { DATABASE_CONNECTION } from '../database/constants';
import { GeoEventSchema } from './schemas/geo-event.schema';

export const GeoEventProviders = [
  {
    provide: GEO_EVENT_MODEL,
    useFactory: (connection: Connection) => connection.model('GeoEvent', GeoEventSchema),
    inject: [DATABASE_CONNECTION]
  }
];
