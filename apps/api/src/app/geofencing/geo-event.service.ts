import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CommandBus } from '@nestjs/cqrs';
import { GeoEventEntity } from './entities/geo-event.entity';
import { GEO_EVENT_MODEL } from './constants';
import { GeoEventDto } from './dto/geo-event.dto';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { DEVICE_MODEL } from '../devices/constants';
import { DeviceEntity } from '../devices/entities/device.entity';
import { PushGeoEventCommand } from './commands/push-geo-event.command';
import { GeoEventType } from './entities/geo-event-type.enum';
import { map } from 'rxjs/operators';

@Injectable()
export class GeoEventService {

  constructor(@Inject(GEO_EVENT_MODEL)
              private readonly geoEventModel: Model<GeoEventEntity>,
              @Inject(DEVICE_MODEL)
              private readonly deviceModel: Model<DeviceEntity>,
              private readonly commandBus: CommandBus) {
  }


  getLatestDeviceGeoEvents(deviceId: string, type?: GeoEventType, limit?: number): Observable<GeoEventEntity[]> {
    const query: any = {
      device: deviceId
    };

    if (type) {
      query.type = type;
    }

    return fromPromise(
      this.geoEventModel.find(query, {}, { sort: { createdAt: 1 } })
        .limit(limit || 5)
        .exec());
  }

  create(geoEventDto: GeoEventDto): Observable<GeoEventEntity> {
    const eventModel = new this.geoEventModel({
      device: geoEventDto.deviceId,
      ...geoEventDto
    });
    return fromPromise(eventModel.save().then(async result => {
      const device = await this.deviceModel.findById(geoEventDto.deviceId);
      device.events.push(eventModel);
      await device.save();
      // Push Event to command bus
      await this.commandBus.execute(new PushGeoEventCommand(result));
      return result;
    }));
  }

  clear(): Observable<number> {
    return fromPromise(this.geoEventModel.deleteMany({}).exec()).pipe(map(results => {
      return results.deletedCount;
    }));
  }
}


