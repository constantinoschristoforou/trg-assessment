import { IsNotEmpty } from 'class-validator';

export class SettingsDto {
  @IsNotEmpty()
  simulatorEnabled: boolean;
}
