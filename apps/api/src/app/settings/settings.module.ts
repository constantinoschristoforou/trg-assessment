import { Module } from '@nestjs/common';
import { SettingsController } from './settings.controller';
import { SettingsService } from './settings.service';
import { DeviceSimulatorModule } from '../device-simulator/device-simulator.module';
import { AlertModule } from '../alerts/alert.module';
import { DeviceModule } from '../devices/device.module';
import { GeofencingModule } from '../geofencing/geofencing.module';

@Module({
  imports: [
    DeviceSimulatorModule,
    AlertModule,
    DeviceModule,
    GeofencingModule
  ],
  controllers: [
    SettingsController
  ],
  providers: [
    SettingsService
  ]
})
export class SettingsModule {
}
