import { Injectable } from '@nestjs/common';
import { Observable, of } from 'rxjs';
import { SettingsDto } from './dto/settings.dto';
import { SettingsEntity } from './entities/settings.entity';
import { SimulatorService } from '../device-simulator/simulator.service';
import { DeviceService } from '../devices/device.service';
import { AlertService } from '../alerts/alert.service';
import { GeoEventService } from '../geofencing/geo-event.service';

@Injectable()
export class SettingsService {

  constructor(private simulatorService: SimulatorService,
              private alertService: AlertService,
              private geoEventService: GeoEventService,
              private deviceService: DeviceService) {
  }

  settings: SettingsEntity = new SettingsEntity();

  get(): Observable<SettingsEntity> {
    return of(this.settings);
  }

  save(settingsDto: SettingsDto): Observable<SettingsEntity> {
    Object.assign(this.settings, settingsDto);

    if (settingsDto.simulatorEnabled) {
      this.simulatorService.startSimulation();
    } else {
      this.simulatorService.stopSimulator();
      this._clearAllData();
    }

    return of(this.settings);
  }

  private _clearAllData() {
    this.deviceService.clearAllLocationData();
    this.alertService.clear();
    this.geoEventService.clear();
  }

}
