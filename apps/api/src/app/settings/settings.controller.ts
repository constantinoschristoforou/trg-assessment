import { Body, Controller, Get, Put } from '@nestjs/common';
import { Observable } from 'rxjs';
import { SettingsService } from './settings.service';
import { SettingsDto } from './dto/settings.dto';
import { SettingsEntity } from './entities/settings.entity';

@Controller('settings')
export class SettingsController {

  constructor(private settingsService: SettingsService) {
  }

  @Get()
  get(): Observable<SettingsEntity> {
    return this.settingsService.get();
  }

  @Put()
  monitor(@Body() settings: SettingsDto): Observable<SettingsEntity> {
    return this.settingsService.save(settings);
  }

}
