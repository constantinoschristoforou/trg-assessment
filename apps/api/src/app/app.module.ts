import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { DeviceModule } from './devices/device.module';
import { CommonModule } from './common/common.module';
import { AreasModule } from './areas/areas.module';
import { DeviceSimulatorModule } from './device-simulator/device-simulator.module';
import { SettingsModule } from './settings/settings.module';
import { AlertModule } from './alerts/alert.module';
import { GeofencingModule } from './geofencing/geofencing.module';

@Module({
  imports: [
    DatabaseModule,
    DeviceModule,
    CommonModule,
    AreasModule,
    DeviceSimulatorModule,
    SettingsModule,
    AlertModule,
    GeofencingModule
  ],
  controllers: [],
  providers: []
})
export class AppModule {
}
