import { catchError, tap } from 'rxjs/operators';
import { NotFoundException } from '@nestjs/common';
import { MonoTypeOperatorFunction, of } from 'rxjs';

export function throwNotFoundException<T>(): MonoTypeOperatorFunction<T> {
  return input$ => input$.pipe(
    catchError(err => {
        throw new NotFoundException();
      }
    ),
    tap((value) => {
      if (!value) {
        throw new NotFoundException();
      }
    }));
}

