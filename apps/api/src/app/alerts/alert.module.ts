import { Module } from '@nestjs/common';
import { AlertController } from './alert.controller';
import { AlertService } from './alert.service';
import { AlertProviders } from './alert.providers';
import { DeviceModule } from '../devices/device.module';
import { DatabaseModule } from '../database/database.module';
import { PushAlertCommand } from './commands/push-alert.command';
import { PushAlertHandler } from './commands/handlers/push-alert.handler';
import { CqrsModule } from '@nestjs/cqrs';
import { AreasModule } from '../areas/areas.module';
import { GeoEventHandler } from './commands/handlers/geo-event.handler';
import { GeofencingModule } from '../geofencing/geofencing.module';

@Module({
  imports: [
    DatabaseModule,
    CqrsModule,
    DeviceModule,
    GeofencingModule,
    AreasModule
  ],
  controllers: [
    AlertController
  ],
  providers: [
    AlertService,
    ...AlertProviders,
    PushAlertCommand,
    PushAlertHandler,
    GeoEventHandler
  ],
  exports: [
    AlertService
  ]
})
export class AlertModule {
}
