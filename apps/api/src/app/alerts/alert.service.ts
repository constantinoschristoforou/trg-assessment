import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { ALERT_MODEL } from './constants';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { AlertEntity } from './entities/alert.entity';
import { AlertDto } from './dto/alert.dto';
import { DEVICE_MODEL } from '../devices/constants';
import { DeviceEntity } from '../devices/entities/device.entity';
import { CommandBus } from '@nestjs/cqrs';
import { PushAlertCommand } from './commands/push-alert.command';
import { AlertType } from './schemas/alert-type.enum';
import { map } from 'rxjs/operators';

@Injectable()
export class AlertService {

  constructor(@Inject(ALERT_MODEL)
              private readonly alertModel: Model<AlertEntity>,
              @Inject(DEVICE_MODEL)
              private readonly deviceModel: Model<DeviceEntity>,
              private readonly commandBus: CommandBus) {
  }

  findAll(conditions = {}): Observable<AlertEntity[]> {
    return fromPromise(
      this.alertModel.find(conditions, {}, { sort: { createdAt: 1 } })
        .populate('device', {
            points: false,
            alerts: false
          }
        )
        .exec());
  }


  getLatestDeviceAlerts(deviceId: string, areaId: string, type?: AlertType, limit?: number): Observable<AlertEntity[]> {
    const query: any = {
      device: deviceId,
      'data.area._id': areaId
    };

    if (type) {
      query.type = type;
    }

    return fromPromise(
      this.alertModel.find(query, {}, { sort: { createdAt: 1 } })
        .limit(limit || 5)
        .exec());
  }

  create(alertDto: AlertDto): Observable<AlertEntity> {
    console.log('results');
    const alertModel = new this.alertModel({
      device: alertDto.deviceId,
      ...alertDto
    });

    return fromPromise(alertModel.save().then(async result => {
      const device = await this.deviceModel.findById(alertDto.deviceId);
      device.alerts.push(alertModel);
      await device.save();
      // Push new alert
      await this.commandBus.execute(new PushAlertCommand(result));
      return result;
    }));
  }

  clear(): Observable<number> {
    return fromPromise(this.alertModel.deleteMany({}).exec()).pipe(map(results => {
      return results.deletedCount;
    }));
  }
}
