import * as mongoose from 'mongoose';
import { AlertType } from './alert-type.enum';

export const AlertSchema = new mongoose.Schema({
    name: String,
    device: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Device'
    },
    type: {
      type: String,
      enum: [AlertType.NEAR_AREA, AlertType.IN_AREA, AlertType.NEAR_DEVICE]
    },
    data: Object
  },
  {
    timestamps: true

  });
