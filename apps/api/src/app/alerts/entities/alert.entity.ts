import { Document } from 'mongoose';
import { DeviceEntity } from '../../devices/entities/device.entity';
import { AlertType } from '../schemas/alert-type.enum';

export interface AlertEntity extends Document {
  name: string;
  device: DeviceEntity;
  type: AlertType,
  data?: Object;
}
