import { AlertEntity } from '../entities/alert.entity';

export class PushAlertCommand {
  constructor(
    public readonly alert: AlertEntity) {
  }
}
