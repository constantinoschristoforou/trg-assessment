import { PushAlertCommand } from '../push-alert.command';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { WebsocketGateway } from '../../../devices/websocket.gateway';
import { ALERT_EVENT } from '../../constants';

@CommandHandler(PushAlertCommand)
export class PushAlertHandler implements ICommandHandler<PushAlertCommand> {
  constructor(private websocketGateway: WebsocketGateway) {
  }

  async execute(command: PushAlertCommand) {
    this.websocketGateway.server.emit(ALERT_EVENT, command.alert);
  }

}
