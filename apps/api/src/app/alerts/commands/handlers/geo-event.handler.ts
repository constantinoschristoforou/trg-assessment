import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { WebsocketGateway } from '../../../devices/websocket.gateway';
import { PushGeoEventCommand } from '../../../geofencing/commands/push-geo-event.command';
import { AlertService } from '../../alert.service';
import { GeoEventEntity } from '../../../geofencing/entities/geo-event.entity';
import { AlertDto } from '../../dto/alert.dto';
import { AlertType } from '../../schemas/alert-type.enum';
import { DeviceEntity } from '../../../devices/entities/device.entity';
import { GeoEventService } from '../../../geofencing/geo-event.service';
import { AreaEntity } from '../../../areas/entities/area.entity';
import { GeoEventType } from '../../../geofencing/entities/geo-event-type.enum';

@CommandHandler(PushGeoEventCommand)
export class GeoEventHandler implements ICommandHandler<PushGeoEventCommand> {
  constructor(private websocketGateway: WebsocketGateway,
              private geoEventService: GeoEventService,
              private alertService: AlertService) {
  }

  async execute(command: PushGeoEventCommand) {
    const event = command.geoEvent;

    switch (event.forEntity) {
      case 'device':
        this._createNearDeviceAlert(event);
        break;
      case 'area':
        this._createGeoFencingAlert(event);
    }
  }


  private async _createGeoFencingAlert(event: GeoEventEntity) {
    const area: AreaEntity = (event.data as any).area;

    // This is only for the demo
    const alertType = event.type === GeoEventType.NEAR ? AlertType.NEAR_AREA : AlertType.IN_AREA;
    const lastEvent = await this.alertService.getLatestDeviceAlerts(event.device, area._id, alertType, 1).toPromise();

    // Event has already generated an alert - do nothing
    if (lastEvent.length) {
      console.log('same event');
      return;
    }

    const alert = this._getGeoFencingAlert(event);
    this.alertService.create(alert);
  }

  private _createNearDeviceAlert(event: GeoEventEntity) {

    const device: DeviceEntity = (event.data as any).device;
    const nearDevice: DeviceEntity = (event.data as any).nearDevice;
    const distance: any = (event.data as any).distance;
    const distanceRounded = parseInt(distance.calculated, 10);

    const alert = new AlertDto({
      name: `${device.name} a is ${distanceRounded}m away from ${nearDevice.name}`,
      type: AlertType.NEAR_DEVICE,
      deviceId: event.device,
      data: event.data
    });

    this.alertService.create(alert);
  }

  private _getGeoFencingAlert(event: GeoEventEntity): AlertDto {
    const device: DeviceEntity = (event.data as any).device;
    const area: AreaEntity = (event.data as any).area;
    const distance: any = (event.data as any).distance;

    let alert = null;

    switch (event.type) {
      case GeoEventType.IN:
        alert = new AlertDto({
          name: `${device.name} is in ${area.name}`,
          type: AlertType.IN_AREA,
          deviceId: event.device,
          data: event.data
        });
        break;

      case GeoEventType.NEAR:
        const distanceRounded = parseInt(distance.calculated, 10);
        alert = new AlertDto({
          name: `${device.name} a is ${distanceRounded} m away from ${area.name} area`,
          type: AlertType.NEAR_AREA,
          deviceId: event.device,
          data: event.data
        });
        break;
    }

    return alert;
  }

}
