import { Body, Controller, Get, Post } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AlertEntity } from './entities/alert.entity';
import { AlertService } from './alert.service';
import { AlertDto } from './dto/alert.dto';

@Controller('alerts')
export class AlertController {

  constructor(private alertService: AlertService) {
  }

  @Get()
  index(): Observable<AlertEntity[]> {
    return this.alertService.findAll();
  }

  @Post()
  create(@Body() alertDto: AlertDto): Observable<AlertEntity> {
    return this.alertService.create(alertDto);
  }
}
