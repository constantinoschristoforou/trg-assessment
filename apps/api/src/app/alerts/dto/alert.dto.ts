import { AlertType } from '../schemas/alert-type.enum';
import { IsNotEmpty } from 'class-validator';

export class AlertDto {

  @IsNotEmpty()
  deviceId: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  type: AlertType;

  data?: Object;

  constructor(data: Partial<AlertDto>) {
    Object.assign(this, data);
  }
}
