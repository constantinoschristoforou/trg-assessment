import { Connection } from 'mongoose';
import { ALERT_MODEL } from './constants';
import { AlertSchema } from './schemas/alert.schema';
import { DATABASE_CONNECTION } from '../database/constants';

export const AlertProviders = [
  {
    provide: ALERT_MODEL,
    useFactory: (connection: Connection) => connection.model('Alert', AlertSchema),
    inject: [DATABASE_CONNECTION]
  }
];
