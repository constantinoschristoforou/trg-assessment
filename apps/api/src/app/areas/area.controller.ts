import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { AreaService } from './area.service';
import { Observable } from 'rxjs';
import { AreaEntity } from './entities/area.entity';
import { AreaDto } from './dto/area.dto';
import { throwNotFoundException } from '../common/rxjs-extensions';

@Controller('areas')
export class AreaController {

  constructor(private areaService: AreaService) {
  }

  @Get()
  index(): Observable<AreaEntity[]> {
    return this.areaService.findAll();
  }

  @Get(':id')
  find(@Param('id') id: string): Observable<AreaEntity> | any {
    return this.areaService.find(id).pipe(throwNotFoundException());
  }

  @Post()
  create(@Body() areaDto: AreaDto): Observable<AreaEntity> {
    return this.areaService.create(areaDto);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() areaDto: AreaDto): Observable<AreaEntity> {
    return this.areaService.update(id, areaDto).pipe(throwNotFoundException());
  }

  @Delete(':id')
  delete(@Param('id') id: string): Observable<AreaEntity> {
    return this.areaService.delete(id).pipe(throwNotFoundException());
  }

}
