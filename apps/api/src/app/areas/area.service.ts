import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { AreaEntity } from './entities/area.entity';
import { AreaDto } from './dto/area.dto';
import { AREA_MODEL } from './constants';
import { map } from 'rxjs/operators';

@Injectable()
export class AreaService {

  constructor(
    @Inject(AREA_MODEL)
    private readonly areaModel: Model<AreaEntity>) {
  }

  create(areaDto: AreaDto): Observable<AreaEntity> {
    return fromPromise(this.areaModel.create(areaDto));
  }

  findAll(conditions: Partial<AreaEntity> | any = {}): Observable<AreaEntity[]> {
    return fromPromise(this.areaModel.find(conditions).exec());
  }

  find(id: string): Observable<AreaEntity> {
    return fromPromise(this.areaModel.findOne({ _id: id }).exec());
  }

  update(id: string, areaDto: AreaDto): Observable<AreaEntity> {
    const updated = this.areaModel.findOneAndUpdate({ _id: id }, areaDto, { new: true }).exec();
    return fromPromise(updated);
  }

  delete(id: string): Observable<AreaEntity> {
    return fromPromise(this.areaModel.findByIdAndDelete(id).exec());
  }

  withinAreas(lngLat: number[]): Observable<AreaEntity[]> {
    return fromPromise(this.areaModel.find({
      'boundaries': {
        '$geoIntersects': {
          '$geometry': {
            'type': 'Point',
            'coordinates': lngLat
          }
        }
      }
    }).exec());
  }

  nearByAreas(lngLat: number[],distance:number=1500): Observable<AreaEntity[]> {
    /**
     *    Exclude the areas that the point is in
     *    Todo: Check if this can be done with one query
     */
    return this.withinAreas(lngLat).pipe(map(withinAreas => {
      return this.areaModel.aggregate([
        {
          $geoNear: {
            near: { type: 'Point', coordinates: lngLat },
            distanceField: 'dist.calculated',
            maxDistance: distance,
            minDistance: 400,
            includeLocs: 'dist.location',
            spherical: true,
            query: { '_id': { '$nin': withinAreas.map(item => item._id) } }
          }
        }
      ]).exec();
    }));
  }

}
