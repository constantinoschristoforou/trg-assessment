import { IsNotEmpty, ValidateNested } from 'class-validator';
import { BoundaryDto } from './boundary.dto';
import { Type } from 'class-transformer';

export class AreaDto {

  @IsNotEmpty()
  name: string;

  enabled: boolean;

  @ValidateNested()
  @IsNotEmpty()
  @Type(() => BoundaryDto)
  boundaries: BoundaryDto;


  description: string;

}
