import { IsNotEmpty } from 'class-validator';

export class BoundaryDto {

  @IsNotEmpty()
  type: string = 'Polygon';

  @IsNotEmpty()
  coordinates: Array<Array<Array<number>>>;

}
