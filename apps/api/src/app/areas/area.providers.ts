import { Connection } from 'mongoose';
import { AreaSchema } from './schemas/area.schema';
import { AREA_MODEL } from './constants';
import { DATABASE_CONNECTION } from '../database/constants';

export const AreaProviders = [
  {
    provide: AREA_MODEL,
    useFactory: (connection: Connection) => connection.model('Area', AreaSchema),
    inject: [DATABASE_CONNECTION]
  }
];
