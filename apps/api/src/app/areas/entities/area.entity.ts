import { Document } from 'mongoose';

export interface AreaEntity extends Document {
  name: string;
  description: string;
  enabled: boolean
  boundaries: {
    coordinates: [];
  }

  dist: {
    calculated: number,
    location: any;
  }
}
