import * as mongoose from 'mongoose';
import { polygonSchema } from '../../database/schemas/polygon.schema';

export const AreaSchema = new mongoose.Schema({
    name: String,
    description: String,
    boundaries: polygonSchema,
    enabled: Boolean
  },
  {
    timestamps: true

  });

// DeviceSchema.indexes();
AreaSchema.index({ 'boundaries': '2dsphere' }, { background: false });
