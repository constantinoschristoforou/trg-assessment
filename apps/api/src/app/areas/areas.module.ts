import { Module } from '@nestjs/common';
import { AreaService } from './area.service';
import { AreaProviders } from './area.providers';
import { DatabaseModule } from '../database/database.module';
import { AreaController } from './area.controller';

@Module({
  imports: [
    DatabaseModule
  ],
  controllers: [
    AreaController
  ],
  providers: [
    AreaService, ...AreaProviders
  ],
  exports: [
    AreaService
  ]
})
export class AreasModule {
}
