import { IsNotEmpty } from 'class-validator';

export class DeviceDto {

  @IsNotEmpty()
  name: string;

  description: string;
}
