import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { DeviceService } from './device.service';
import { DeviceDto } from './dto/device.dto';
import { Observable } from 'rxjs';
import { DeviceEntity } from './entities/device.entity';
import '../common/rxjs-extensions';
import { throwNotFoundException } from '../common/rxjs-extensions';
import { MonitorAction } from './entities/monitor-action.enum';
import { LocationEntity } from './entities/location.entity';
import { LocationDto } from './dto/location.dto';

@Controller('devices')
export class DeviceController {

  constructor(private deviceService: DeviceService) {
  }

  @Get()
  index(): Observable<DeviceEntity[]> {
    return this.deviceService.findAll();
  }

  @Put('monitor')
  monitor(@Body() data: { ids: string[], action: MonitorAction }): Observable<number> {
    return this.deviceService.monitor(data.ids, data.action);
  }

  @Get(':id')
  find(@Param('id') id: string): Observable<DeviceEntity> | any {
    return this.deviceService.find(id).pipe(throwNotFoundException());
  }

  @Post()
  create(@Body() deviceDto: DeviceDto): Observable<DeviceEntity> {
    return this.deviceService.create(deviceDto);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() deviceDto: DeviceDto): Observable<DeviceEntity> {
    return this.deviceService.update(id, deviceDto).pipe(throwNotFoundException());
  }

  @Delete(':id')
  delete(@Param('id') id: string): Observable<DeviceEntity> {
    return this.deviceService.delete(id).pipe(throwNotFoundException());
  }

  @Get(':id/locations')
  getDeviceLocations(@Param('id') deviceId): Observable<LocationEntity[]> {
    return this.deviceService.getLocations(deviceId);
  }

  @Get(':id/locations/last')
  getDeviceLastKnownLocation(@Param('id') deviceId): Observable<LocationEntity> {
    return this.deviceService.getLastKnownLocation(deviceId);
  }

  @Post(':id/locations')
  addLocation(@Param('id') deviceId, @Body() locationDto: LocationDto): Observable<LocationEntity> {
    return this.deviceService.addLocation(deviceId, locationDto);
  }


}
