import { Document } from 'mongoose';
import { MonitorAction } from './monitor-action.enum';
import { LocationEntity } from './location.entity';
import { AlertEntity } from '../../alerts/entities/alert.entity';
import { GeoEventEntity } from '../../geofencing/entities/geo-event.entity';

export interface DeviceEntity extends Document {
  name?: string;
  description?: string;
  monitorAction?: MonitorAction;
  monitorActionAt?: Date;
  points?: LocationEntity[];
  lastPoint?: LocationEntity;
  alerts?: AlertEntity[];
  events?: GeoEventEntity[];

  dist: {
    calculated: number,
    location: any;
  }
}
