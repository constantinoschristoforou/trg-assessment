import { Document } from 'mongoose';

export interface LocationEntity extends Document {
  device: string;
  point: {
    coordinates: number[];
    type: string;
  }
}
