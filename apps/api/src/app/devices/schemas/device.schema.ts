import * as mongoose from 'mongoose';
import { MonitorAction } from '../entities/monitor-action.enum';
import { DeviceLocationSchema } from './device-location.schema';

export const DeviceSchema = new mongoose.Schema({
    name: String,
    description: String,
    monitorAction: {
      type: String,
      enum: [MonitorAction.ACTIVE, MonitorAction.INACTIVE, MonitorAction.PAUSED]
    },
    monitorActionAt: Date,
    points: [{ type: mongoose.Schema.Types.ObjectId, ref: 'DeviceLocation' }],
    alerts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Alert' }],
    events: [{ type: mongoose.Schema.Types.ObjectId, ref: 'GeoEvent' }],
    lastPoint: DeviceLocationSchema
  },
  {
    timestamps: true
  });

DeviceSchema.index({ 'lastPoint': '2dsphere' }, { background: false });
