import * as mongoose from 'mongoose';
import { pointSchema } from '../../database/schemas/point.schema';

export const DeviceLocationSchema = new mongoose.Schema({
    point: pointSchema,
    device: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Device'
    }
  },
  {
    timestamps: true
  });

DeviceLocationSchema.index({ 'point': '2dsphere' }, { background: false });
