import { Connection } from 'mongoose';
import { DeviceSchema } from './schemas/device.schema';
import { DeviceLocationSchema } from './schemas/device-location.schema';
import { DEVICE_LOCATION_MODEL, DEVICE_MODEL } from './constants';
import { DATABASE_CONNECTION } from '../database/constants';

export const DevicesProviders = [
  {
    provide: DEVICE_MODEL,
    useFactory: (connection: Connection) => connection.model('Device', DeviceSchema),
    inject: [DATABASE_CONNECTION]
  },
  {
    provide: DEVICE_LOCATION_MODEL,
    useFactory: (connection: Connection) => connection.model('DeviceLocation', DeviceLocationSchema),
    inject: [DATABASE_CONNECTION]
  }
];
