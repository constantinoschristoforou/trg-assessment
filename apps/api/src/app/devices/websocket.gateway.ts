import { SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';

@WebSocketGateway()
export class WebsocketGateway {
  @WebSocketServer() server;

  @SubscribeMessage('messages')
  async onMessage(client, data: string): Promise<string> {
    this.server.emit('messages', 'testform server');
    return data + 'from server';
  }
}
