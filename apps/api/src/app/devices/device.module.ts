import { Module } from '@nestjs/common';
import { DeviceController } from './device.controller';
import { DeviceService } from './device.service';
import { DevicesProviders } from './device.providers';
import { DatabaseModule } from '../database/database.module';
import { WebsocketGateway } from './websocket.gateway';
import { CqrsModule } from '@nestjs/cqrs';
import { PushDeviceLocationCommand } from './commands/push-device-location.command';
import { PushDeviceLocationHandler } from './commands/handlers/push-device-location.handler';
import { GeofencingCommand } from './commands/geofencing.command';
import { AreasModule } from '../areas/areas.module';

@Module({
  imports: [
    DatabaseModule,
    CqrsModule,
    AreasModule
  ],
  controllers: [
    DeviceController
  ],
  providers: [
    DeviceService,
    ...DevicesProviders,
    WebsocketGateway,
    PushDeviceLocationCommand,
    PushDeviceLocationHandler,
    GeofencingCommand

  ],
  exports: [
    DeviceService,
    WebsocketGateway,
    ...DevicesProviders
  ]
})
export class DeviceModule {
}
