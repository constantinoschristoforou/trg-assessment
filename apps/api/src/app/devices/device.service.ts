import { Model } from 'mongoose';
import { Inject, Injectable } from '@nestjs/common';
import { DeviceDto } from './dto/device.dto';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { DeviceEntity } from './entities/device.entity';
import { MonitorAction } from './entities/monitor-action.enum';
import { map, tap } from 'rxjs/operators';
import { LocationDto } from './dto/location.dto';
import { LocationEntity } from './entities/location.entity';
import { DEVICE_LOCATION_MODEL, DEVICE_MODEL } from './constants';
import { CommandBus } from '@nestjs/cqrs';
import { PushDeviceLocationCommand } from './commands/push-device-location.command';

@Injectable()
export class DeviceService {
  constructor(
    @Inject(DEVICE_MODEL)
    private readonly deviceModel: Model<DeviceEntity>,
    @Inject(DEVICE_LOCATION_MODEL)
    private readonly locationModel: Model<LocationEntity>,
    private readonly commandBus: CommandBus) {
  }

  create(deviceDto: DeviceDto): Observable<DeviceEntity> {
    const deviceModel = new this.deviceModel(deviceDto);
    return fromPromise(deviceModel.save());
  }

  findAll(conditions: Object = {}): Observable<DeviceEntity[]> {
    return fromPromise(
      this.deviceModel.find(conditions, null, { sort: { monitorAction: 1 } })
        .populate({
          path: 'points',
          options: {
            limit: 1,
            sort: { created: -1 }
          }
        }).select({ alerts: false, events: false }).exec());
  }

  find(id: string): Observable<DeviceEntity> {
    return fromPromise(this.deviceModel.findOne({ _id: id }, {
      points: false,
      alerts: false
    }).exec());
  }

  update(id: string, deviceDto: DeviceDto): Observable<DeviceEntity> {
    const updated = this.deviceModel.findOneAndUpdate({ _id: id }, deviceDto, {
      new: true,
      'fields': { points: false, alerts: false }
    }).exec();
    return fromPromise(updated);
  }

  delete(id: string): Observable<DeviceEntity> {
    return fromPromise(this.deviceModel.findByIdAndDelete(id).exec());
  }

  clearAllLocationData(): Observable<number> {
    return fromPromise(this.locationModel.deleteMany({}).exec()).pipe(map(results => {
      return results.deletedCount;
    }));
  }

  monitor(ids: string[], action: MonitorAction): Observable<number> {
    const updated: any = this.deviceModel.updateMany({ '_id': { $in: ids } }, {
      monitorAction: action,
      monitorActionAt: new Date()
    } as DeviceEntity).exec();
    return fromPromise(updated).pipe(map((result: any) => result.n));
  }

  getLocations(deviceId: string): Observable<LocationEntity[]> {
    return fromPromise(this.deviceModel.findById(deviceId).populate('points').exec()).pipe(map(item => item.points));
  }

  getLastKnownLocation(deviceId: string): Observable<LocationEntity> {
    return fromPromise(this.locationModel.findOne({ 'device': deviceId }).sort('-created_at').exec());
  }

  addLocation(deviceId: string, locationDto: LocationDto): Observable<LocationEntity> {
    const locationModel = new this.locationModel({
      device: deviceId,
      point: {
        type: 'Point',
        coordinates: locationDto.coordinates
      }
    });
    return fromPromise(locationModel.save()).pipe(tap(async result => {
      const device = await this.deviceModel.findById(deviceId);
      device.points.push(locationModel);

      device.lastPoint = result;

      await device.save();
      await this.commandBus.execute(new PushDeviceLocationCommand(result));
      return result;
    }));
  }

  nearByDevices(lngLat: number[], distance: number = 1000, exclude: string[] = []): Observable<DeviceEntity[]> {
    return fromPromise(this.deviceModel.aggregate([
      {
        $geoNear: {
          near: { type: 'Point', coordinates: lngLat },
          distanceField: 'dist.calculated',
          maxDistance: distance,
          minDistance: 0,
          includeLocs: 'dist.location',
          spherical: true,
          query: { '_id': { '$nin': exclude } }
        }
      }
    ]).exec());
  }

}
