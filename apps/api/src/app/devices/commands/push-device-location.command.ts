import { LocationEntity } from '../entities/location.entity';

export class PushDeviceLocationCommand {
  constructor(
    public readonly location: LocationEntity) {
  }
}
