import { LocationEntity } from '../entities/location.entity';

export class GeofencingCommand {
  constructor(
    public readonly location: LocationEntity) {
  }
}
