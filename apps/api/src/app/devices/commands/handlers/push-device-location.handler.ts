import { PushDeviceLocationCommand } from '../push-device-location.command';
import { CommandBus, CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { WebsocketGateway } from '../../websocket.gateway';
import { DEVICE_LOCATION_EVENT } from '../../constants';
import { GeofencingCommand } from '../geofencing.command';

@CommandHandler(PushDeviceLocationCommand)
export class PushDeviceLocationHandler implements ICommandHandler<PushDeviceLocationCommand> {
  constructor(private websocketGateway: WebsocketGateway,
              private readonly commandBus: CommandBus) {
  }

  async execute(command: PushDeviceLocationCommand) {
    this.websocketGateway.server.emit(DEVICE_LOCATION_EVENT, command.location);
    await this.commandBus.execute(new GeofencingCommand(command.location));
  }
}
