import { Module } from '@nestjs/common';
import { ScheduleModule } from 'nest-schedule';
import { DeviceModule } from '../devices/device.module';
import { DirectionsService } from './directions.service';
import { SimulatorService } from './simulator.service';

@Module({
  imports: [
    ScheduleModule.register(),
    DeviceModule
  ],
  providers: [
    DirectionsService,
    SimulatorService
  ],
  exports: [
    SimulatorService
  ]
})
export class DeviceSimulatorModule {
}
