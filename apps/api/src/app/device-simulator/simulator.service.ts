import { Injectable } from '@nestjs/common';
import { IJobConfig, InjectSchedule, NestSchedule, Schedule } from 'nest-schedule';
import * as turf from '@turf/turf';
import { DeviceService } from '../devices/device.service';
import { DirectionsService } from './directions.service';
import { LocationDto } from '../devices/dto/location.dto';
import { DeviceRoute } from './device-route.class';
import { MonitorAction } from '../devices/entities/monitor-action.enum';
import { DeviceEntity } from '../devices/entities/device.entity';

@Injectable()
export class SimulatorService extends NestSchedule {

  boundaryBox = [32.6541, 34.703, 33.5738, 35.305];
  deviceRoutes: Map<string, DeviceRoute> = new Map();
  devices: DeviceEntity[] = [];

  constructor(private deviceService: DeviceService,
              private directionService: DirectionsService,
              @InjectSchedule() private readonly schedule: Schedule) {
    super();
  }

 async startSimulation(): Promise<any> {
    const options = { waiting: true, immediate: true };
    console.log('Start Simulator');
    this.schedule.scheduleIntervalJob('simulator', 1000, async () => {
      console.log('Running');

      const devices= await   this.deviceService.findAll({ monitorAction: MonitorAction.ACTIVE }).toPromise();

      devices.forEach(device => {
        this._pingRoutePoint(device);
      }, options);

      return false;
    });
  }

  stopSimulator() {
    console.log('Stop');
    this.schedule.cancelJob('simulator');
  }

  private _pingRoutePoint(device: DeviceEntity) {

    const deviceRoute = this.deviceRoutes.get(String(device._id));
    let nextRoutePoint = null;

    if (deviceRoute) {
      nextRoutePoint = deviceRoute.getNextPoint();
    }

    if (deviceRoute && nextRoutePoint) {
      const locationDto = new LocationDto();
      locationDto.coordinates = nextRoutePoint;

      this.deviceService.addLocation(device._id, locationDto).subscribe();
    } else {
      this._generatedFakeRoute(device._id, deviceRoute ? deviceRoute.getLastPoint() : null);
    }
  }

  private _generatedFakeRoute(deviceId: string, origin?: number[]) {

    if (!origin) {
      origin = turf.randomPosition({ bbox: this.boundaryBox as any });
    }

    const destination = turf.randomPosition({ bbox: this.boundaryBox as any });

    this.directionService.getDirections(origin, destination).subscribe(route => {
      this.deviceRoutes.set(String(deviceId), new DeviceRoute({
        route
      }));
    });
  }
}
