import { Injectable } from '@nestjs/common';
import { createClient, GoogleMapsClient } from '@google/maps';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { map } from 'rxjs/operators';
import * as polyline from 'google-polyline';


@Injectable()
export class DirectionsService {

  client: GoogleMapsClient;

  constructor() {
    this.client = createClient({
      key: 'AIzaSyBuh6JLFmXorRgPvwsh6Ov4Uy5oAiDi9w8',
      Promise: Promise
    });
  }

  getDirections(origin: number[], destination: number[]): Observable<Array<Array<number>>> {
    const request = this.client.directions({
      origin: [origin[1], origin[0]] as any,
      destination: [destination[1], destination[0]] as any
    }).asPromise();
    return fromPromise(request).pipe(map(result => {
      if (result.json.routes && result.json.routes.length) {
        return polyline.decode(result.json.routes[0].overview_polyline.points).map(latLng => {
          return [latLng[1], latLng[0]];
        });
      }
      return [];
    }));
  }

}
