export class DeviceRoute {
  route: Array<Array<number>>;
  lastPointIndex: number = -1;

  constructor(partial: Partial<DeviceRoute> = {}) {
    Object.assign(this, partial);
  }

  getNextPoint(): number[] {
    this.lastPointIndex++;
    return this.lastPointIndex < this.route.length ? this.route[this.lastPointIndex] : null;
  }

  getLastPoint(): number[] {
    return !this.route ? null : this.route[this.route.length - 1];
  }
}
