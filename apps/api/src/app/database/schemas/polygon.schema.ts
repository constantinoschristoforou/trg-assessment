import * as mongoose from 'mongoose';
import { AreaSchema } from '../../areas/schemas/area.schema';

export const polygonSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ['Polygon'],
    required: true
  },
  coordinates: {
    type: [[[Number]]],
    required: true
  }
});
