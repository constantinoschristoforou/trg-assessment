module.exports = {
  name: 'device-tracking',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/device-tracking',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
