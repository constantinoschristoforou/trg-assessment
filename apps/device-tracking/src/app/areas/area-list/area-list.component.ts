import { Component } from '@angular/core';
import { Area } from '../shared/area.class';
import { AreaService } from '../shared/area.service';
import { MatSnackBar } from '@angular/material';
import { SidebarService } from '../../core/sidebar.service';
import { MapService } from '../../map/shared/map.service';
import { Router } from '@angular/router';

@Component({
  selector: 'assessment-area-list',
  templateUrl: './area-list.component.html',
  styleUrls: ['./area-list.component.scss']
})
export class AreaListComponent {

  areas: Area[];

  constructor(private areaService: AreaService,
              private sidebarService: SidebarService,
              private mapService: MapService,
              private router: Router,
              private snackBar: MatSnackBar) {
    this._loadAreas();
  }

  showOnMap(area: Area) {
    if (area.enabled) {
      this.mapService.openInfoWindow(area._id, 'area');
    }
  }

  create() {
    this.router.navigate([{ outlets: { sidebar: 'area/create' } }]);
  }

  action(id: string, action: 'enable' | 'disable' | 'delete' | 'edit' ) {

    switch (action) {

      case 'enable':
      case 'disable':
        const area = this.areas.find(item => item._id === id);
        area.enabled = action === 'enable';
        this._update(area);

        break;
      case 'delete':
        this._delete(id);
        break;

      case 'edit':
        this.router.navigate([{ outlets: { sidebar: `area/${id}/edit` } }]);
        break;
    }
  }

  private _loadAreas() {
    this.areaService.getAreas().subscribe(areas => {
      this.areas = areas;
      this.mapService.drawAreas(areas);
    });
  }

  private _update(area: Area) {
    this.areaService.save(area).subscribe(response => {
      this.snackBar.open('Area successfully saved.');
      this.mapService.drawAreas(this.areas);
    }, error => {
      this.snackBar.open('Error saving area.');
    });
  }

  private _delete(id: string) {
    this.areaService.delete(id).subscribe(response => {
      this.areas = this.areas.filter(item => item._id !== id);
      this.mapService.drawAreas(this.areas);
      this.snackBar.open('Area successfully deleted.');
    }, error => {
      this.snackBar.open('Error deleting area.');
    });
  }

}
