import { Component } from '@angular/core';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { MapService } from '../../map/shared/map.service';
import { Area } from '../shared/area.class';
import { AreaService } from '../shared/area.service';
import { MatSnackBar } from '@angular/material';
import { SidebarService } from '../../core/sidebar.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '../../shared/base-component.class';

@Component({
  selector: 'assessment-edit-area-drawer',
  templateUrl: './edit-area-drawer.component.html',
  styleUrls: ['./edit-area-drawer.component.scss']
})

export class EditAreaDrawerComponent extends BaseComponent {

  form = new FormGroup({});
  area: Area = new Area();
  fields: FormlyFieldConfig[] = [];
  selectedOverlay: google.maps.OverlayView;
  formOptions: FormlyFormOptions = {};

  constructor(private mapService: MapService,
              private areaService: AreaService,
              private sidebarService: SidebarService,
              private snackBar: MatSnackBar,
              private router: Router,
              private route: ActivatedRoute) {
    super();
    this._setupForm();
    this._subscribeOnDrawingCompleted();
    this.area._id = route.snapshot.params.id;

    if (this.area._id) {
      this._loadArea();
    }

    this.sidebarService.open('right');
  }

  drawingMode() {
    this.mapService.drawingManager.show();
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    // if is create mode and area is not selected
    if (!this.area._id && !this.selectedOverlay) {
      return;
    }

    this._save();
  }

  clearPolygon() {
    this.selectedOverlay.setMap(null);
    this.selectedOverlay = null;
  }

  close() {
    this.sidebarService.close('right');
    this.mapService.drawingManager.hide();
    this.router.navigate([{ outlets: { sidebar: null } }]);
  }

  private _setupForm() {
    this.fields = [{
      key: 'name',
      type: 'input',
      templateOptions: {
        label: 'Name',
        placeholder: 'Name',
        required: true
      }
    },
      {
        key: 'description',
        type: 'textarea',
        templateOptions: {
          label: 'Description',
          placeholder: 'Description',
          rows: 3,
          required: true
        }
      },
      {
        key: 'enabled',
        type: 'toggle',
        defaultValue: true,
        templateOptions: {
          label: 'Enabled',
          required: true
        }
      }
    ];
  }

  private _save() {
    this.saving = true;

    // Get area path
    if (this.selectedOverlay) {
      const path = (this.selectedOverlay as any).getPaths().getArray()[0].getArray();

      path.forEach(item => {
        const lat = item.lat();
        const lng = item.lng();
        this.area.boundaries.coordinates[0].push([lng, lat]);
      });

      // Push first point as last, polygon is not close otherwise
      this.area.boundaries.coordinates[0].push([path[0].lng(), path[0].lat()])
    }

    this.areaService.save(this.area).subscribe(response => {
      this.snackBar.open('Area successfully saved.');

      this.mapService.drawingManager.clearSelectedOverlay();
      this.mapService.drawingManager.hide();

      // Refresh data - not optimal but good for now
      this.areaService.getAll().subscribe();

      // Clear form
      this.formOptions.resetModel();

      this.close();
      this.saving = false;

    }, error => {
      this.snackBar.open('Error saving area.');
      this.saving = false;
    });

  }

  private _subscribeOnDrawingCompleted() {
    this.mapService.drawingManager.drawingCompleted$.subscribe(overlay => {
      this.selectedOverlay = overlay;
      this.mapService.drawingManager.hide();
    });
  }


  private _loadArea() {
    this.areaService.find(this.area._id).subscribe(area => {
      this.area = area;
    });
  }
}
