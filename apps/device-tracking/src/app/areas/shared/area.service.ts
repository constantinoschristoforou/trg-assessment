import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/base-service.class';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Area } from './area.class';
import { filter, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AreaService extends BaseService {

  resourcePath = 'areas';

  private activeDataSubject = new BehaviorSubject<Area[]>(null);

  constructor(private http: HttpClient) {
    super();
  }

   getAreas(): Observable<Area[]> {
    return this.activeDataSubject.asObservable().pipe(filter(item => !!item));
  }

  updateSelectedData(data: Area[]) {
    this.activeDataSubject.next(data);
  }

  getAll(): Observable<Area[]> {
    return this.http.get<Area[]>(this.url).pipe(tap(areas => this.updateSelectedData(areas)));
  }

  find(id: string): Observable<Area> {
    return this.http.get<Area>(this.url + '/' + id);
  }

  save(area: Area): Observable<Area> {
    return area._id ? this.update(area) : this.create(area);
  }

  create(area: Area): Observable<Area> {
    return this.http.post<Area>(this.url, area);
  }

  update(area: Area): Observable<Area> {
    return this.http.put<Area>(this.getResourceUrl(area._id), area);
  }

  delete(id: string): Observable<Area> {
    return this.http.delete<Area>(this.getResourceUrl(id));
  }

}
