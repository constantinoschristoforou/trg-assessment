import { Boundary } from './boundary.class';

export class Area {
  _id: string;
  name: string;
  enabled: boolean;
  boundaries: Boundary = new Boundary();
  description: string;
}
