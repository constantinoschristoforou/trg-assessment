export class Boundary {
  type: string = 'Polygon';
  coordinates: Array<Array<Array<number>>>;

  constructor() {
    this.coordinates = [[]];
  }
}

