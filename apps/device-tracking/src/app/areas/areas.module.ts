import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaListComponent } from './area-list/area-list.component';
import {
  MatButtonModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatRippleModule,
  MatSnackBarModule,
  MatTooltipModule
} from '@angular/material';
import { EditAreaDrawerComponent } from './edit-area-drawer/edit-area-drawer.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '../shared/forms/forms.module';

@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatRippleModule,
    MatSnackBarModule,
    MatButtonModule,
    MatTooltipModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    AreaListComponent,
    EditAreaDrawerComponent
  ],
  exports: [
    AreaListComponent,
    EditAreaDrawerComponent
  ]
})
export class AreasModule {
}
