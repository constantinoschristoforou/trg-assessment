import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { MapService } from './shared/map.service';
import { BaseComponent } from '../shared/base-component.class';
import { WebsocketService } from '../shared/websocket.service';
import { Device } from '../devices/shared/device.class';
import { DeviceService } from '../devices/shared/device.service';

@Component({
  selector: 'assessment-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent extends BaseComponent implements AfterViewInit {

  @ViewChild('mapContainer', { static: false }) gmap: ElementRef;

  devices: Device[] = [];
  map: google.maps.Map;

  mapOptions: google.maps.MapOptions = {
    center: new google.maps.LatLng(35.1149052, 33.3261645),
    zoom: 10,
    disableDefaultUI: true,
    zoomControl:true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.SMALL,
      position: google.maps.ControlPosition.LEFT_TOP
    },
  };

  constructor(private mapService: MapService,
              private deviceService: DeviceService,
              private websocketService: WebsocketService) {
    super();
    this._subscribeToDeviceLocations();
    this._subscribeToDevices();
  }

  ngAfterViewInit(): void {
    this._setupMap();
  }

  private _setupMap() {
    this.map = new google.maps.Map(this.gmap.nativeElement,
      this.mapOptions);

    this.mapService.map = this.map;
    this.mapService.drawingManager.map = this.map;
    this.mapService.drawingManager.hide();
  }

  private _subscribeToDevices() {
    this.subscription = this.deviceService.getDevices().subscribe(devices => {
      this.devices = devices;
    });
  }

  private _subscribeToDeviceLocations() {
    this.subscription = this.websocketService.getDeviceLocations().subscribe(location => {
      const device = this.devices.find(item => item._id === location.device);
      this.mapService.updateDeviceLocation(device, location);
    });
  }
}

