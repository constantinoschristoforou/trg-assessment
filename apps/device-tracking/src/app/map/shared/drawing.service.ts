import { EventEmitter, Injectable } from '@angular/core';
import OverlayType = google.maps.drawing.OverlayType;

@Injectable({
  providedIn: 'root'
})
export class DrawingService {

  drawingCompleted$: EventEmitter<any> = new EventEmitter<any>();
  selectedOverlay: google.maps.OverlayView;
  manager: google.maps.drawing.DrawingManager;
  private _map: google.maps.Map;

  set map(value: google.maps.Map) {
    this._map = value;
    this.manager.setMap(value);
  }

  constructor() {
    this._init();
  }

  clearSelectedOverlay() {
    if(this.selectedOverlay){
      this.selectedOverlay.setMap(null);
    }
  }

  show() {
    this.manager.setMap(this._map);
  }

  hide() {
    this.manager.setMap(null);
  }

  private _init() {
    this.manager = new google.maps.drawing.DrawingManager({
      drawingMode: OverlayType.POLYGON,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_LEFT,
        drawingModes: [OverlayType.POLYGON]
      },
      markerOptions: { icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png' },
      circleOptions: {
        fillColor: '#ffff00',
        fillOpacity: 1,
        strokeWeight: 5,
        clickable: false,
        editable: true,
        zIndex: 1
      }
    });

    google.maps.event.addListener(this.manager, 'overlaycomplete', (event => {
      this.selectedOverlay = event.overlay;
      this.drawingCompleted$.emit(event.overlay);
    }));
  }


}
