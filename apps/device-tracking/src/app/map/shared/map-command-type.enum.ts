export enum MapCommandType {
  POLYGON_SELECTION = 'polygon-selection',
  MONITORING = 'monitor',
  CLEAR_POLYGON = 'clear-polygon',
}
