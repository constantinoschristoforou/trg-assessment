import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { MapCommand } from './map-command.class';
import { DrawingService } from './drawing.service';
import { Area } from '../../areas/shared/area.class';
import { Device } from '../../devices/shared/device.class';
import { MonitorAction } from '../../devices/shared/monitor-action.enum';
import { DeviceLocation } from '../../devices/shared/device-location.class';
import LatLng = google.maps.LatLng;
import Polygon = google.maps.Polygon;
import InfoWindow = google.maps.InfoWindow;
import Marker = google.maps.Marker;
import MVCObject = google.maps.MVCObject;

@Injectable({
  providedIn: 'root'
})
export class MapService {

  get map(): google.maps.Map {
    return this._map;
  }

  set map(value: google.maps.Map) {
    this._map = value;
  }

  get command(): Observable<MapCommand> {
    return this._command.asObservable();
  }

  constructor(public drawingManager: DrawingService) {
  }

  private _map: google.maps.Map;
  private _command: Subject<MapCommand> = new Subject<MapCommand>();
  private areaPolygons: Polygon[] = [];
  private deviceMarkers: Marker[] = [];

  static isInfoWindowOpen(infoWindow: InfoWindow | any) {
    const map = infoWindow.getMap();
    return (map !== null && typeof map !== 'undefined');
  }

  clearAreas() {
    this.areaPolygons.forEach(item => {
      // Close info window first
      (item.get('infoWindow') as InfoWindow).close();
      item.setMap(null);
    });
  }

  drawAreas(areas: Area[]) {
    // Clear existing ones first
    this.clearAreas();

    for (const area of areas) {

      if (!area.enabled) {
        continue;
      }
      // Convert to google maps lat lng
      const coords = area.boundaries.coordinates[0].map(item => {
        return new LatLng(item[1], item[0]);
      });

      const polygon = new google.maps.Polygon({
        paths: coords,
        strokeColor: 'rgba(18,12,18,0.58)',
        strokeOpacity: 0.2,
        strokeWeight: 1,
        fillColor: 'rgba(18,12,18,0.62)',
        fillOpacity: 0.35
      });

      const infoWindow = new google.maps.InfoWindow({
        content: `<h4>${area.name}</h4 <div class="mt-2">${area.description}</div>`,
        position: polygon.getPath().getArray()[0]
      });

      google.maps.event.addListener(polygon, 'click', (e) => {
        infoWindow.setPosition(e.latLng);
        infoWindow.open(this.map);
      });

      // Set custom properties
      polygon.set('id', area._id);
      polygon.set('infoWindow', infoWindow);
      polygon.setMap(this.map);

      this.areaPolygons.push(polygon);
    }
  }


  addDevices(devices: Device[]) {
    for (const device of devices) {
      if (!device.points.length || device.monitorAction !== MonitorAction.ACTIVE) {
        continue;
      }
      this._createDeviceMarker(device);
    }
  }

  updateDeviceLocation(device: Device, location: DeviceLocation) {
    const marker = this.deviceMarkers.find(item => item.get('id') === location.device);
    if (!marker) {
      device.points.unshift(location);
      this._createDeviceMarker(device);
    } else {
      const latLng = new LatLng(location.point.coordinates[1], location.point.coordinates[0]);
      marker.setPosition(latLng);
      (marker.get('infoWindow') as InfoWindow).setPosition(latLng);
    }
  }

  openInfoWindow(id: string, type: 'device' | 'area') {

    const overlayArray: MVCObject[] = type === 'device' ? this.deviceMarkers : this.areaPolygons;
    const overlay = overlayArray.find(item => item.get('id') === id);

    if (overlay) {
      const infoWindow = (overlay.get('infoWindow') as InfoWindow);
      if (!MapService.isInfoWindowOpen(infoWindow)) {
        infoWindow.open(this.map);
        this.map.panTo(infoWindow.getPosition());
        this.map.setZoom(type === 'area' ? 12 : 15);
      } else {
        infoWindow.close();
        this.map.setZoom(11);
      }
    }
  }

  private _createDeviceMarker(device: Device): Marker {
    const latLng = device.points[0].point.coordinates;
    const marker = new google.maps.Marker({
      position: new LatLng(latLng[1], latLng[0]),
      map: this.map,
      icon: {
        path: google.maps.SymbolPath.CIRCLE,
        fillColor: '#00000f',
        fillOpacity: 0.6,
        strokeColor: '#00000f',
        strokeOpacity: 0.9,
        strokeWeight: 1,
        scale: 8
      }
    });

    const infoWindow = new google.maps.InfoWindow({
      content: `<h4>${device.name}</h4 <div class="mt-2">${device.description}</div>`,
      position: marker.getPosition()
    });

    google.maps.event.addListener(marker, 'click', (e) => {
      infoWindow.setPosition(e.latLng);
      infoWindow.open(this.map, marker);
    });

    // Set custom properties
    marker.set('id', device._id);
    marker.set('infoWindow', infoWindow);
    this.deviceMarkers.push(marker);

    return marker;
  }


}
