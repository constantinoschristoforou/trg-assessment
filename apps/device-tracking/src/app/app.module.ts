import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RoutingModule } from './app.routing';
import { CoreModule } from './core/core.module';
import { MatButtonModule, MatIconModule, MatSidenavModule, MatTabsModule } from '@angular/material';
import { MapModule } from './map/map.module';
import { DevicesModule } from './devices/devices.module';
import { AreasModule } from './areas/areas.module';
import { SharedModule } from './shared/shared.module';
import { SettingsModule } from './settings/settings.module';
import { AlertsModule } from './alerts/alerts.module';

@NgModule({
  imports: [
    RoutingModule,
    CoreModule,
    MatSidenavModule,
    MatButtonModule,
    MapModule,
    MatTabsModule,
    MatIconModule,
    DevicesModule,
    AreasModule,
    SharedModule,
    SettingsModule,
    AlertsModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
