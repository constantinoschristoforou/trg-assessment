import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertListComponent } from './alert-list/alert-list.component';
import {
  MatButtonModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatRippleModule,
  MatSnackBarModule,
  MatTooltipModule
} from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { MomentModule } from 'ngx-moment';

@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatRippleModule,
    MatSnackBarModule,
    MatButtonModule,
    MatTooltipModule,
    SharedModule,
    MomentModule
  ],
  declarations: [
    AlertListComponent
  ],
  exports: [
    AlertListComponent
  ]

})
export class AlertsModule {
}
