import { AlertType } from './alert-type.enum';
import { Device } from '../../devices/shared/device.class';

export class Alert {
  name: string;
  type: AlertType;
  device: Device;
  data?: Object;
  createdAt: Date;
}
