export enum AlertType {
  NEAR_DEVICE = 'near-device',
  NEAR_AREA = 'near-area',
  IN_AREA = 'in-area',
}
