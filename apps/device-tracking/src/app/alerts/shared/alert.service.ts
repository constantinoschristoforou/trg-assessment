import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/base-service.class';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { filter, tap } from 'rxjs/operators';
import { Alert } from './alert.class';

@Injectable({
  providedIn: 'root'
})
export class AlertService extends BaseService {

  resourcePath = 'alerts';

  private activeDataSubject = new BehaviorSubject<Alert[]>(null);

  constructor(private http: HttpClient) {
    super();
  }

  getAlerts(): Observable<Alert[]> {
    return this.activeDataSubject.asObservable().pipe(filter(item => !!item));
  }

  updateSelectedData(data: Alert[]) {
    this.activeDataSubject.next(data);
  }

  getAll(): Observable<Alert[]> {
    return this.http.get<Alert[]>(this.url).pipe(tap(alerts => this.updateSelectedData(alerts)));
  }

}
