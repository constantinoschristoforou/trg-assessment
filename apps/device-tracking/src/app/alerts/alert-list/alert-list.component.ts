import { Component } from '@angular/core';
import { Alert } from '../shared/alert.class';
import { AlertService } from '../shared/alert.service';
import { AlertType } from '../shared/alert-type.enum';
import { MapService } from '../../map/shared/map.service';
import { WebsocketService } from '../../shared/websocket.service';
import { MatSnackBar } from '@angular/material';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'assessment-alert-list',
  templateUrl: './alert-list.component.html',
  styleUrls: ['./alert-list.component.scss']
})
export class AlertListComponent {

  alerts: Alert[] = [];
  alertType = AlertType;

  constructor(private alertService: AlertService,
              private websocketService: WebsocketService,
              private snackBar: MatSnackBar,
              private mapService: MapService) {
    this._loadAlerts();
    this._subscribeToNewAlerts();
  }

  viewDetails(alert: Alert) {

  }

  showDeviceOnMap(alert: Alert) {
    this.mapService.openInfoWindow(alert.device._id, 'device');
  }

  private _loadAlerts() {
    this.alertService.getAlerts().subscribe(alerts => {
      this.alerts = alerts;
    });
  };

  private _subscribeToNewAlerts() {
    this.websocketService.getAlerts().subscribe(alert => {
      this.alerts.unshift(alert);
      this._showAlertSnackBar(alert);
    });
  }

  private _showAlertSnackBar(alert: Alert) {
    const titleCasePipe = new TitleCasePipe();
    const title = titleCasePipe.transform(alert.type.replace('-', ' ')) + ' Alert: ' + alert.name;
    const snackBarRef = this.snackBar.open(title, 'View', {
      duration: 10000,
      panelClass: 'danger'
    });

    snackBarRef.onAction().subscribe(() => {
      // todo : navigate to alert details
      snackBarRef.dismiss();
    });


  }

}
