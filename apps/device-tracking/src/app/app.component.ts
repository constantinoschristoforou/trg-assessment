import { Component, OnInit, ViewChild } from '@angular/core';
import { SidebarService } from './core/sidebar.service';
import { BaseComponent } from './shared/base-component.class';
import { MatDrawer } from '@angular/material';
import { AreaService } from './areas/shared/area.service';
import { WebsocketService } from './shared/websocket.service';
import { DeviceService } from './devices/shared/device.service';
import { AlertService } from './alerts/shared/alert.service';
import { SettingsService } from './settings/shared/settings.service';

@Component({
  selector: 'assessment-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends BaseComponent implements OnInit {

  @ViewChild('leftDrawer', { static: true }) leftDrawer: MatDrawer;
  @ViewChild('rightDrawer', { static: true }) rightDrawer: MatDrawer;

  drawers: Map<string, MatDrawer> = new Map();

  constructor(private sidebar: SidebarService,
              private websocketService: WebsocketService,
              private areaService: AreaService,
              private alertService: AlertService,
              private settingsService: SettingsService,
              private deviceService: DeviceService) {
    super();

    this._loadData();
    this._subscribeOnReloadData();
  }

  ngOnInit(): void {
    this.drawers.set('left', this.leftDrawer);
    this.drawers.set('right', this.rightDrawer);
    this._handleSidebar();
  }

  private _handleSidebar() {
    this.subscription = this.sidebar.action.subscribe(action => {
      this.websocketService.sendMessage('test');
      switch (action.state) {
        case 'close':
          this.drawers.get(action.sidebar).close();
          break;
        case 'open':
          this.drawers.get(action.sidebar).open();
          break;
        case 'toggle':
          this.drawers.get(action.sidebar).toggle();
          break;
      }
    });
  }

  private _loadAreas() {
    this.areaService.getAll().subscribe();
  }

  private _loadDevices() {
    this.deviceService.getAll().subscribe();
  }

  private _loadAlerts() {
    this.alertService.getAll().subscribe();
  }

  private _loadData() {
    this._loadAreas();
    this._loadDevices();
    this._loadAlerts();
  }

  private _subscribeOnReloadData() {
    this.settingsService.reloadData$.subscribe(() => {
      this._loadData();
    });
  }
}
