import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditAreaDrawerComponent } from './areas/edit-area-drawer/edit-area-drawer.component';
import { EditDeviceDrawerComponent } from './devices/edit-device-drawer/edit-device-drawer.component';
import { EditSettingsDrawerComponent } from './settings/edit-settings-drawer/edit-settings-drawer.component';

const routes: Routes = [
  // Areas
  { path: 'area/create', component: EditAreaDrawerComponent, outlet: 'sidebar' },
  { path: 'area/:id/edit', component: EditAreaDrawerComponent, outlet: 'sidebar' },

  //  Devices
  { path: 'device/create', component: EditDeviceDrawerComponent, outlet: 'sidebar' },
  { path: 'device/:id/edit', component: EditDeviceDrawerComponent, outlet: 'sidebar' },
  { path: 'settings', component: EditSettingsDrawerComponent, outlet: 'sidebar' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      enableTracing: false,
      paramsInheritanceStrategy: 'always'
    }
  )],
  exports: [RouterModule],
  providers: []
})
export class RoutingModule {
}
