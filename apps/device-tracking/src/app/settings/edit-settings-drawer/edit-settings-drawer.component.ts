import { Component } from '@angular/core';
import { BaseComponent } from '../../shared/base-component.class';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { SidebarService } from '../../core/sidebar.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Settings } from '../shared/settings.class';
import { SettingsService } from '../shared/settings.service';

@Component({
  selector: 'assessment-edit-settings-drawer',
  templateUrl: './edit-settings-drawer.component.html',
  styleUrls: ['./edit-settings-drawer.component.scss']
})

export class EditSettingsDrawerComponent extends BaseComponent {

  form = new FormGroup({});
  settings: Settings = new Settings();
  fields: FormlyFieldConfig[] = [];
  formOptions: FormlyFormOptions = {};

  constructor(private settingsService: SettingsService,
              private sidebarService: SidebarService,
              private snackBar: MatSnackBar,
              private router: Router) {
    super();
    this._setupForm();
    this._loadSettings();
    this.sidebarService.open('right');
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this._save();
  }

  close() {
    this.sidebarService.close('right');
    this.router.navigate([{ outlets: { sidebar: null } }]);
  }

  private _setupForm() {
    this.fields = [
      {
        key: 'simulatorEnabled',
        type: 'toggle',
        defaultValue: true,
        templateOptions: {
          label: 'Enable Simulator',
          required: true,
          description: 'Generate random paths in Cyprus for all of the devices.By disabling the simulator all data will be cleared! '
        }
      }
    ]
    ;
  }

  private _save() {
    this.saving = true;
    this.settingsService.save(this.settings).subscribe(() => {
      this.snackBar.open('Settings successfully saved.');
      this.close();
      this.saving = false;
    }, () => {
      this.snackBar.open('Error saving settings.');
      this.saving = false;
    });
  }

  private _loadSettings() {
    this.settingsService.get().subscribe(settings => {
      this.settings = settings;
    });
  }
}
