import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Settings } from './settings.class';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../shared/base-service.class';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SettingsService extends BaseService {
  resourcePath = 'settings';

  reloadData$: Subject<boolean> = new Subject<boolean>();

  constructor(private http: HttpClient) {
    super();
  }

  get(): Observable<Settings> {
    return this.http.get<Settings>(this.url);
  }

  save(settings: Settings): Observable<Settings> {
    return this.http.put<Settings>(this.url, settings).pipe(tap(result => {

      // Refresh Data
      if (!result.simulatorEnabled) {
        this.reloadData$.next(true);
      }

    }));
  }
}
