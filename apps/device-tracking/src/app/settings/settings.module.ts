import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditSettingsDrawerComponent } from './edit-settings-drawer/edit-settings-drawer.component';
import {
  MatButtonModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatRippleModule,
  MatSnackBarModule, MatTooltipModule
} from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '../shared/forms/forms.module';

@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatRippleModule,
    MatSnackBarModule,
    MatButtonModule,
    MatTooltipModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    EditSettingsDrawerComponent
  ]
})
export class SettingsModule {
}
