import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatButtonModule, MatIconModule, MatToolbarModule } from '@angular/material';
import { AngularSplitModule } from 'angular-split';
import { HttpClientModule } from '@angular/common/http';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { Configuration } from './config.class';

const config: SocketIoConfig = { url: Configuration.WEB__SOCKET_URL, options: {} };

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    SocketIoModule.forRoot(config),
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    AngularSplitModule.forRoot(),
    MatButtonModule
  ],
  declarations: [
    ToolbarComponent
  ],
  exports: [
    ToolbarComponent
  ],
  providers: [
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'end'
      }
    }
  ]
})
export class CoreModule {
}
