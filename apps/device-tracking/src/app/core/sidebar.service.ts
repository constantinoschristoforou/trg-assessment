import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


export type SidebarPosition = 'left' | 'right';
export type SidebarAction = 'open' | 'close' | 'toggle';

export interface SidebarStage {
  sidebar: SidebarPosition;
  state: SidebarAction
}


@Injectable({
  providedIn: 'root'
})

export class SidebarService {

  private _action: Subject<SidebarStage> = new Subject<SidebarStage>();

  get action(): Observable<SidebarStage> {
    return this._action.asObservable();
  }

  open(sidebar: 'left' | 'right') {
    this._action.next({ sidebar: sidebar, state: 'open' });
  }

  close(sidebar: 'left' | 'right') {
    this._action.next({ sidebar: sidebar, state: 'close' });
  }

  toggle(sidebar: 'left' | 'right') {
    this._action.next({ sidebar: sidebar, state: 'toggle' });
  }


}
