import { environment } from '../../environments/environment';

export class Configuration {
  static API_URL = environment.url;
  static WEB__SOCKET_URL = environment.websocketUrl;
  static MAPS_KEY = environment.mapKey;
}

