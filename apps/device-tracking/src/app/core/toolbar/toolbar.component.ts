import { Component } from '@angular/core';
import { SidebarService } from '../sidebar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'assessment-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {

  constructor(private sidebar: SidebarService,
              private router: Router) {
  }

  toggleSidebar() {
    this.sidebar.toggle('left');
  }

  settings() {
    this.router.navigate([{ outlets: { sidebar: 'settings' } }]);
    this.sidebar.toggle('right');
  }

}
