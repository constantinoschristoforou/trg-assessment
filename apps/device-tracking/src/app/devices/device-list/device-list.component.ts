import { Component } from '@angular/core';
import { DeviceService } from '../shared/device.service';
import { Device } from '../shared/device.class';
import { MonitorAction } from '../shared/monitor-action.enum';
import { MatSnackBar } from '@angular/material';
import { MapService } from '../../map/shared/map.service';
import { Router } from '@angular/router';

@Component({
  selector: 'assessment-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss']
})
export class DeviceListComponent {

  devices: Device[] = [];

  monitorAction = MonitorAction;

  constructor(private deviceService: DeviceService,
              private mapService: MapService,
              private router: Router,
              private snackBar: MatSnackBar) {
    this._loadDevices();
  }

  viewAlerts(id: string) {
  }

  showOnMap(device: Device) {
    this.mapService.openInfoWindow(device._id, 'device');
  }

  edit(id: string) {
    this.router.navigate([{ outlets: { sidebar: `device/${id}/edit` } }]);
  }

  create() {
    this.router.navigate([{ outlets: { sidebar: 'device/create' } }]);
  }

  delete(id: string) {
    this.deviceService.delete(id).subscribe(response => {
      this.devices = this.devices.filter(item => item._id !== id);
      this.snackBar.open('Device successfully deleted.');
    }, error => {
      this.snackBar.open('Error deleting device.');
    });
  }

  action(id: string, action: MonitorAction) {
    this.deviceService.monitor([id], action).subscribe(result => {
      const changed = this.devices.find(item => item._id === id);
      changed.monitorAction = action;
    });
  }

  private _loadDevices() {
    this.deviceService.getDevices().subscribe(devices => {
      this.devices = devices;
      this.mapService.addDevices(this.devices);
    });
  }


}
