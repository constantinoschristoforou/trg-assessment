import { Component } from '@angular/core';
import { SidebarService } from '../../core/sidebar.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { BaseComponent } from '../../shared/base-component.class';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { Device } from '../shared/device.class';
import { DeviceService } from '../shared/device.service';

@Component({
  selector: 'assessment-edit-device-drawer',
  templateUrl: './edit-device-drawer.component.html',
  styleUrls: ['./edit-device-drawer.component.scss']
})

export class EditDeviceDrawerComponent extends BaseComponent {

  form = new FormGroup({});
  device: Device = new Device();
  fields: FormlyFieldConfig[] = [];
  formOptions: FormlyFormOptions = {};

  constructor(private deviceService: DeviceService,
              private sidebarService: SidebarService,
              private snackBar: MatSnackBar,
              private router: Router,
              private route: ActivatedRoute) {
    super();
    this._setupForm();
    this.device._id = route.snapshot.params.id;

    if (this.device._id) {
      this._loadDevice();
    }
    this.sidebarService.open('right');
  }


  submit() {
    if (this.form.invalid) {
      return;
    }

    this._save();
  }


  close() {
    this.sidebarService.close('right');
    this.router.navigate([{ outlets: { sidebar: null } }]);
  }

  private _setupForm() {
    this.fields = [{
      key: 'name',
      type: 'input',
      templateOptions: {
        label: 'Name',
        placeholder: 'Name',
        required: true
      }
    },
      {
        key: 'description',
        type: 'textarea',
        templateOptions: {
          label: 'Description',
          placeholder: 'Description',
          rows: 3,
          required: true
        }
      }
    ];
  }

  private _save() {
    this.saving = true;
    this.deviceService.save(this.device).subscribe(response => {
      this.snackBar.open('Device successfully saved.');
      // Clear form
      this.formOptions.resetModel();

      // Refresh data - not optimal but good for now
      this.deviceService.getAll().subscribe();

      this.close();
      this.saving = false;
    }, error => {
      this.snackBar.open('Error saving device.');
      this.saving = false;
    });
  }

  private _loadDevice() {
    this.deviceService.find(this.device._id).subscribe(device => {
      this.device = device;
    });
  }
}
