import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/base-service.class';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Device } from './device.class';
import { MonitorAction } from './monitor-action.enum';
import { DeviceLocation } from './device-location.class';
import { filter, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DeviceService extends BaseService {

  resourcePath = 'devices';

  private activeDataSubject = new BehaviorSubject<Device[]>(null);

  constructor(private http: HttpClient) {
    super();
  }

  getDevices(): Observable<Device[]> {
    return this.activeDataSubject.asObservable().pipe(filter(item => !!item));
  }

  updateSelectedData(data: Device[]) {
    this.activeDataSubject.next(data);
  }

  getAll(): Observable<Device[]> {
    return this.http.get<Device[]>(this.url).pipe(tap(devices => this.updateSelectedData(devices)));
  }

  find(id: string): Observable<Device> {
    return this.http.get<Device>(this.url + '/' + id);
  }

  save(device: Device): Observable<Device> {
    return device._id ? this.update(device) : this.create(device);
  }

  create(device: Device): Observable<Device> {
    return this.http.post<Device>(this.url, device);
  }

  update(device: Device): Observable<Device> {
    return this.http.put<Device>(this.getResourceUrl(device._id), device);
  }

  delete(id: string): Observable<Device> {
    return this.http.delete<Device>(this.getResourceUrl(id));
  }

  monitor(ids: string[] | string, action: MonitorAction): Observable<number> {
    return this.http.put<number>(this.url + '/monitor', { ids, action });
  }

  getLastKnownLocation(id: string): Observable<DeviceLocation> {
    return this.http.get<DeviceLocation>(this.getResourceUrl(id + '/locations/last'));
  }

}
