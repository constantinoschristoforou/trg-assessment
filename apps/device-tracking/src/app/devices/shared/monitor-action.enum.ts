export enum MonitorAction {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
  PAUSED = 'paused',
}
