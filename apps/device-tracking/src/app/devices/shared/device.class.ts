import { MonitorAction } from './monitor-action.enum';
import { DeviceLocation } from './device-location.class';

export class Device {
  _id: string;
  name: string;
  monitorAction: MonitorAction = MonitorAction.ACTIVE;
  monitorActionAt: Date;
  points: DeviceLocation[];
  description: string;
  createdAt: Date;
  updateAt: Date;
}
