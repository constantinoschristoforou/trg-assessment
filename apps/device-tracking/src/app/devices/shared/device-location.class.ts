export class DeviceLocation {
  device: string;
  point: {
    coordinates: number[];
    type: string;
  };
  createdAt: Date;
  updatedAt: Date;

}
