import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeviceListComponent } from './device-list/device-list.component';
import {
  MatButtonModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatRippleModule,
  MatSnackBarModule, MatTooltipModule
} from '@angular/material';
import { EditDeviceDrawerComponent } from './edit-device-drawer/edit-device-drawer.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '../shared/forms/forms.module';


@NgModule({
  imports: [
    CommonModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatRippleModule,
    MatSnackBarModule,
    MatButtonModule,
    MatTooltipModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    DeviceListComponent,
    EditDeviceDrawerComponent
  ],
  exports: [
    DeviceListComponent
  ]
})
export class DevicesModule {
}
