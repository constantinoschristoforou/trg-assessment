import { Configuration } from '../core/config.class';

export class BaseService {
  baseUrl: string = Configuration.API_URL;
  resourcePath: string;

  get url(): string {
    return this.baseUrl + this.resourcePath;
  }

  getResourceUrl(id: string): string {
    return this.url + '/' + id;
  }
}
