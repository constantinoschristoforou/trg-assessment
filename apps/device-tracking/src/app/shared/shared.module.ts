import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrawerPanelComponent } from './components/drawer-panel/drawer-panel.component';
import { MatDividerModule, MatIcon, MatIconModule, MatProgressSpinnerModule, MatSpinner } from '@angular/material';
import { SpinBtnDirective } from './directives/spin-btn.directive';

@NgModule({
  imports: [
    CommonModule,
    MatDividerModule,
    MatIconModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    DrawerPanelComponent,
    SpinBtnDirective
  ],
  exports: [
    DrawerPanelComponent,
    SpinBtnDirective
  ],
  entryComponents: [
    MatSpinner,
    MatIcon
  ]
})
export class SharedModule {
}
