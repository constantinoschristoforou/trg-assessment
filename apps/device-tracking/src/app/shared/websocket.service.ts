import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Observable } from 'rxjs';
import { DeviceLocation } from '../devices/shared/device-location.class';
import { Alert } from '../alerts/shared/alert.class';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService implements OnGatewayConnection, OnGatewayDisconnect {

  constructor(private socket: Socket) {
  }

  sendMessage(msg: string) {
    this.socket.emit('messages', msg);
  }

  getDeviceLocations(): Observable<DeviceLocation> {
    return this.socket.fromEvent<any>('DEVICE_LOCATION_EVENT');
  }

  getAlerts(): Observable<Alert> {
    return this.socket.fromEvent<any>('ALERT_EVENT');
  }

  close() {
    this.socket.disconnect();
  }

  async handleConnection() {
    console.log('handleConnection');
  }

  async handleDisconnect() {
    console.log('handleDisconnect');
  }

}
