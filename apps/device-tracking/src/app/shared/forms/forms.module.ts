import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormlyMatToggleModule } from '@ngx-formly/material/toggle';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';

@NgModule({
  imports: [
    CommonModule,
    FormlyMatToggleModule,
    ReactiveFormsModule,
    FormlyModule.forRoot(),
    FormlyMaterialModule
  ],
  declarations: [],
  exports: [
    ReactiveFormsModule,
    FormlyModule,
    FormlyMaterialModule
  ]
})
export class FormsModule {
}
