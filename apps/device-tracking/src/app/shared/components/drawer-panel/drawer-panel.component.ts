import { Component, Input } from '@angular/core';
import { SidebarPosition, SidebarService } from '../../../core/sidebar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'assessment-drawer-panel',
  templateUrl: './drawer-panel.component.html',
  styleUrls: ['./drawer-panel.component.scss']
})
export class DrawerPanelComponent {

  @Input()
  title: string = '';

  @Input()
  icon: string = 'edit';

  @Input()
  type: SidebarPosition;

  constructor(private sidebarService: SidebarService,
              private router: Router) {
  }

  close() {
    this.sidebarService.close(this.type);
    this.router.navigate([{ outlets: { sidebar: null } }]);
  }

}
