# TRG assessment

This project was generated using [Nx](https://nx.dev).
# Requirements 

## Back-end:
1. Expose a REST endpoint that allows creating GEOJSON polygons with a certain name.
2. Expose websocket that produces heartbeats (updates) of N (at least 3) of the following type:
Updates
a) new location of each entity are sent every 5 seconds
b) a notification about an entity entering inside a polygon area. (Just a text with the name of
the entity and the area name)
Notifications are sent to the clients whenever an entity crosses into any of the polygons.
4. (Bonus) If 2 or more devices are in a distance less than 10km between them, send an extra
type of notification called 'proximity'
which contains the names of the devices that are nearby.
5. (Bonus) Use a reactive approach (eg. rxJS) to solve the problem, wherever possible.
Notes:
- Each entity is moving randomly inside Cyprus (rough accuracy is fine), independently of each
other.
- The notifications are 1 per device (regardless how many polygons it intersects, the information
should be inside 1 notification).
## Front-end:
1. A client subscribes to a feed of location updates and notifications for a selected entity.
3. Offer the ability to subscribe/unsubscribe at runtime to the feed of several entities.
4. (Bonus) Offer the ability for slow network connections to receive updates every 10 or 15
seconds.
5. (Bonus) Offer the ability to 'mute' notifications for a specific device for 8 hours.
4. (Bonus) Containerize the two services using Docker.

# Implementation Details

## System Components
- Device Module
    - Rest crud api for devices
    - Websocket server for alerts & device data
- Areas Module 
    - Rest crud api for areas
- GeoFencing Module
    -   Process raw location events and generated higher level of events (called GeoEvents)
        - Near Area/Device
        - In Area
- Alert Module
    -   Process GeoEvents and generate alerts
    The idea is to be able to generate enter/leaving area alerts but for the demo i am firing in and near alerts
- Device Simulator

## Simulate Device geo location events
 - Get random origin and destination geo point - need boundaries of Cyprus for that: 
 https://boundingbox.klokantech.com/
 - Use google directions api to get overview_polyline (encoded) - needs to be decoded first
 use https://www.npmjs.com/package/google-polyline library

 ### Simulator Algorithm
  - For active devices get find a route and store the polyline to mongo db, also store the latest location
  - Fire the next location to Device Location service
  - if the route finish then generate an new location from the last geo point
 
## Tools
- http://geojson.io/
-  https://boundingbox.klokantech.com/

## Documentation
Postman json is included in documentation directory.
## Demo 
View screenshots and video on
https://drive.google.com/drive/folders/1BRXI0IBrGAmGUiMNStJlHMomLic9nWRa?usp=sharing






